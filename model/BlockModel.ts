export interface StakeUnit {
    stakeUnits?: string;
}

export interface PoolBlock {
    balance_rune: number;
    balance_asset: number;
    asset: string;
    pool_units: number;
    status?: string;
    time?: string;
    height: number;
    price?: PoolPrice
}

export interface PoolPrice {
    BUSD_PRICE_IN_RUNE: number;
    ASSET_PRICE_IN_RUNE: number;
    RUNE_PRICE_IN_BUSD: number;
    RUNE_PRICE_IN_ASSET: number;
    ASSET_PRICE_IN_BUSD: number;
}


export interface LastBlock {
    chain: string;
    lastobservedin: number;
    lastsignedout: string;
    thorchain: number;
}

export interface BlockHeight {
    result: Result;
}
export interface Result {
    block: Block;
}

export interface Block {
    header: Header;
    data: Data;
}

export interface Data {
    txs?: string[];
}

export interface Header {
    time: string;
}
