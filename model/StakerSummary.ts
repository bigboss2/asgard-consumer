export interface StakePoolSummary {
    asset: string;
    stake: PoolStakeAmount;
    withdraw?: PoolStakeAmount;
    time?: string;
}

export interface PoolStakeAmount {
    asset: string;
    assetAmount: string;
    targetAmount: string;
    totalAssetAmount: string;
    totalTargetAmount: string;
    totalBUSDAmount: string;
    unit?: string;
}
