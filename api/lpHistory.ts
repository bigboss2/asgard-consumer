import {NowRequest, NowResponse} from "@vercel/node";
 const LPHistory = require('../Calculations/LPHistory')


export default async (request: NowRequest, response: NowResponse) => {

     try {
         const {pool} = request.query
         const {address} = request.query
         console.log(request.headers.origin);
         console.log(pool)
         console.log(address)
         const result = await LPHistory.getLPHistory(address, pool)
         response.setHeader("Access-Control-Allow-Origin", "*");
         response.status(200).send(result)
     }catch (e){
         console.log('Error ' + e.toString())
         response.setHeader("Access-Control-Allow-Origin", "*");
         response.status(500).send(e)
     }


}
