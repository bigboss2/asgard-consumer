import {NowRequest, NowResponse} from "@vercel/node";
import {BlockData} from "../model/StakerState";
import {ips} from "../model/ips"
const axios = require("axios");
// const fastcsv = require('fast-csv');
// const fs = require('fs');
import { PrismaClient } from '@prisma/client'
// or const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

export default async (request: NowRequest, response: NowResponse) => {

const query = request.query
    const pool = query.pool
    let height = query.height
    const isNeedTime = query.isNeedTime
    console.log('calling BUSD-BD1')

    let ip = await getServerIp()
    if (ip === undefined){
        response.status(500).send({error:'No ip found.'})
        return
    }
    let BUSD = await getPoolData(pool,height,(isNeedTime === 'false' || isNeedTime === undefined) ? false : true, ip)
    console.log(BUSD)

    response.setHeader("Access-Control-Allow-Origin", "*");
    if (BUSD !== undefined){
        response.status(200).send(BUSD)
    }else {
        response.status(500).send({error:'No data found.'})
    }

}

const getServerIp = async() => {

    try {
        // const ips = await prisma.$queryRaw<ips[]>('SELECT address FROM public.server_ips order by time limit 10;');
        // // @ts-ignore
        // console.log(ips)
        // return ips[(Math.random()*ips.length)|0].address

        const ENDPOINT = ":1317/thorchain/nodeaccounts"
        const seedlist  = await axios.get('https://chaosnet-seed.thorchain.info')
        let nodelist = await axios.get(`http://${seedlist.data[0]}${ENDPOINT}`)
        nodelist = nodelist.data;

        // Get pool address from all nodes.
        let activeAccounts = nodelist.filter(x => x.status == 'active').map(value => {
            return {ip: value.ip_address, status: value.status}
        })
        console.log(activeAccounts)
        const randomIp = activeAccounts[0]
        console.log(randomIp)
        return randomIp.ip

    } catch(e){
        console.log(e)
        return;
    }
}

const getPoolData = async(ASSET, HEIGHT, ISNEEDTIME: boolean, ip) => {
    try {
        console.log('calling BUSD-BD1')
        let heightnew = HEIGHT
        if (ASSET === 'BNB.BUSD-BD1' && parseInt(HEIGHT) <= 128909){
            console.log('calling BUSD-BD1 ' + heightnew)
            heightnew = '128909'
        }
        const POOL_URL = `http://${ip}:1317/thorchain/pool/${ASSET}?height=${heightnew}`
        const BLOCK_URL = `http://${ip}:27147/block?height=${heightnew}`;
        let path = []
        console.log('loading ' + HEIGHT);
        console.log('url ' + POOL_URL);

        path.push(axios.get(POOL_URL))
        if (ISNEEDTIME) {
            path.push(axios.get(BLOCK_URL))
        }
        const result = await axios.all(path)
   //    console.log(result)

        const pool:PoolBlock = result[0].data
        console.log(pool)
        if (ISNEEDTIME){
            const res:BlockData = result[1].data
            pool.time = res.result.block.header.time
        }

        pool.height = parseInt(HEIGHT)
        return pool

    } catch(e){
       console.log(e)
        return;
    }
}


interface StakeUnit {
    stakeUnits?: string;
}

interface PoolBlock {
    balance_rune?: number;
    balance_asset?: number;
    asset?: string;
    pool_units?: number;
    status?: string;
    time?: string;
    height: number;
}


interface Block {
    result?: Result;
}
interface Result {
    block?: Block;
}

interface Block {
    header?: Header;
    data?: Data;
}

interface Data {
    txs?: string[];
}

interface Header {
    time?: string;
}



