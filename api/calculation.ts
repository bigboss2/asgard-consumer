import {NowRequest, NowResponse} from "@vercel/node";
import {StakerTransaction} from "../model/StakerTransaction";
const axios = require("axios");
import { PrismaClient } from '@prisma/client'
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
const queryBuilder = require('./query.js')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

const BUSD = 'BNB.BUSD-BD1';

export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    const address = query.address
    const ASSET = query.pool

    let txs:StakerTransaction[] = await getTranscationData(ASSET, address).catch(error => {
        response.status(500).send({error: "Error getting transaction data."})
        return
    })

    let heights:string[] = []
    let USDHeight:string[] = []
    let duplicateUSD: string[] = []
    console.log(txs)
    for (const tx of txs.reverse()){
        if (tx.asset === BUSD) {
            USDHeight.push(tx.height)
        }else {
            heights.push(tx.height)
            duplicateUSD.push(tx.height)
        }
    }

    let poolData:PoolBlock[] = await prisma.$queryRaw(queryBuilder.blockPool(heights.concat(USDHeight), ASSET)).catch(error => {
        console.log(error)
        console.log('change to use API')
        return []
    });
    console.log('moving one from get pool')

    USDHeight = USDHeight.concat(duplicateUSD)
    console.log(USDHeight)

    poolData.forEach(value => {
        if (value.asset === BUSD){
            USDHeight = USDHeight.filter(item => item !== value.height.toString());
            console.log(USDHeight)
        }else {
            heights = heights.filter(item => item !== value.height.toString());
            console.log(heights)
        }
    })

    const queryPoolMap = createPoolMap(poolData)
    let poolMap: { [height: string]: PoolBlock } = queryPoolMap[0]
    let poolBUSDMap: { [height: string]: PoolBlock } = queryPoolMap[1]

    if (heights.concat(USDHeight).length > 0 ){
        const temp = heights.concat(USDHeight)
        let uniqueHeight = temp.filter((c, index) => {
            return temp.indexOf(c) === index;
        });
        console.log('calling these height from api: ' + JSON.stringify(uniqueHeight,null,8))
        const poolFromAPI: PoolBlock[] = await getAllPoolData(ASSET,uniqueHeight.length, uniqueHeight)
        //console.log('result pool ' + JSON.stringify(poolFromAPI,null,8))
       poolData = poolData.concat(poolFromAPI)
    }

    let priceMap:  { [height: string]: Price } = {};
    let result: StakerTxDetail[] = [];

    for (const value of poolData){
    //    console.log(value);
        if (value.asset === BUSD) {
            poolBUSDMap[value.height] = value
        }else {
            poolMap[value.height] = value
        }
    }

    txs.forEach(function (value) {
        const price = getPrice(value.asset === BUSD ?  poolBUSDMap[value.height]:  poolMap[value.height], poolBUSDMap[value.height])
        const poolShare = doCalculation(poolMap[value.height], value)
       // const poolMape = poolMap[]
        const stakerTxDetail: StakerTxDetail = {
            // poolHistory: value.asset === BUSD ?  poolBUSDMap[value.height]:  poolMap[value.height],
          // poolShare: poolShare,
            price: price,
            stakeTransaction: value
        }
        result.push(stakerTxDetail)
    })

    const data: StakerTxState = {
        count: result.length,
        pool: `${ASSET}`,
        txDetail: result
    }

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send(data)
}

function createPoolMap(poolData: PoolBlock[]){
    let poolMap: { [height: string]: PoolBlock } = {};
    let poolBUSDMap: { [height: string]: PoolBlock } = {};
    for (const value of poolData){
      //  console.log(value);
        if (value.asset === BUSD) {
            poolBUSDMap[value.height] = value
        }else {
            poolMap[value.height] = value
        }
    }
    return [poolMap, poolBUSDMap]
}

function getPrice(ASSETPOOL: PoolBlock, BUSD: PoolBlock): Price {
    console.log('asset pool ' + JSON.stringify(ASSETPOOL))
    console.log('BUSD pool ' + JSON.stringify(BUSD))
    if (ASSETPOOL === undefined && BUSD === undefined) {
        return
    }

    //  What is the price of the asset in RUNE?
    let BUSD_RUNEPRICE = BUSD.balance_rune / BUSD.balance_asset
    let BNB_RUNEPRICE = ASSETPOOL.balance_rune / ASSETPOOL.balance_asset
    console.log(`AT HEIGHT: ${ASSETPOOL.height}`)

    console.log(`BUSD PRICE IN RUNE: ${BUSD_RUNEPRICE.toFixed(4)}`)
    console.log(`BNB PRICE IN RUNE: ${BNB_RUNEPRICE.toFixed(4)}`)

    // What is the price of RUNE in the asset?
    let BUSD_ASSETPRICE = BUSD.balance_asset / BUSD.balance_rune
    let BNB_ASSETPRICE =   ASSETPOOL.balance_asset / ASSETPOOL.balance_rune

    console.log(`RUNE PRICE IN BUSD: ${BUSD_ASSETPRICE.toFixed(4)}`)
    console.log(`RUNE PRICE IN BNB: ${BNB_ASSETPRICE.toFixed(4)}`)

    // What is the price of one asset in another?
    let BNB_BUSDPRICE =  BNB_RUNEPRICE / BUSD_RUNEPRICE

    console.log(`BNB PRICE IN BUSD: ${BNB_BUSDPRICE.toFixed(4)}`)

    return {
        asset: ASSETPOOL.asset,
        assetPriceInBUSD:BNB_BUSDPRICE,
        assetPriceInRune: BNB_RUNEPRICE,
        runePriceInAsset: BNB_ASSETPRICE,
        runePriceInBUSD:BUSD_ASSETPRICE,
        busdPriceInRune: BUSD_RUNEPRICE
    }

}

function doCalculation(ASSETPOOL: PoolBlock, staker: StakerTransaction): StakerPoolShare {
    const baseNumber = Math.pow(10, 8); // 1e8
    if (ASSETPOOL === undefined) {
        return
    }

    let stakeUnitsBN = staker.stakeunit
    let runeDepthBN = ASSETPOOL.balance_rune
    let assetDepthBN = ASSETPOOL.balance_asset
    let poolUnit = ASSETPOOL.pool_units

    let runeShare = (runeDepthBN * stakeUnitsBN)/poolUnit
    let assetShare = (assetDepthBN * stakeUnitsBN)/poolUnit

    console.log(`runeShare: ${(runeShare / baseNumber).toFixed(4)}`)
    console.log(`assetShare: ${(assetShare / baseNumber).toFixed(4)}`)

    return {
        runeShare: runeShare,
        assetShare: assetShare,
        asset: ASSETPOOL.asset
    }
}



const getTranscationData = async (ASSET, ADDRESS) => {
    try {
        const TXS_URL = `https://asgard-consumer.vercel.app/api/staker?address=${ADDRESS}&pool=${ASSET}`;
        const TXS = await axios.get(TXS_URL)
        return TXS.data.data
    } catch(e){
        //    console.log(e)
        return;
    }
}

const getAllPoolData = async(ASSET, TOTAL: number, HEIGHT: string[]) => {

    let path = []

  //  console.log('calling count: ' + TOTAL.length + ' total: ' + TOTAL + ' REMain : ' + TOTAL.length)

    const  limit = 10;

    const pageNo = Math.ceil(TOTAL/limit);
    let results  = []
    var i;
    for (i = 0; i < pageNo; i++) {
        const heights = HEIGHT.slice(i * limit,(i * limit) + limit)
        console.log('calling height ' + heights)
        for (const height of heights){
            if (ASSET !== BUSD) {
                const BUSD_URL = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=BNB.BUSD-BD1&height=${height}`
                path.push(axios.get(BUSD_URL))
            }
            const POOL_URL = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=${ASSET}&height=${height}`
            path.push(axios.get(POOL_URL))

        }
        const response = await axios.all(path)
        //   console.log(path)
        for (const data of response) {
            results.push(data.data)
        }
    }

    console.log('length ' + results.length + 'vs  total: ' + TOTAL)
  //  console.log(results)
    return results
}


const stall = async(ms=1500) => {await new Promise(resolve => setTimeout(resolve, ms));
}


