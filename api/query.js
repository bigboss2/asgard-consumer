const SqlString = require('sqlstring');
const queryBuilder = function queryBuild(req, type, datatype) {
    console.log(req.query)
    const asset = req.query.asset;
    const from = req.query.from;
    const to = req.query.to;

    if (datatype === 'pool'){
        queryString = 'SELECT ' +
        'asset, ' +
        'extract(epoch from bucket) as bucket, ' +
        'avgassetdepth, ' +
        'CAST ( assetdepth AS varchar ), ' +
        'avgrunedepth,  ' +
        'CAST ( runedepth AS varchar ), ' +
        'avgpoolunits, ' +
        'CAST ( poolunits AS varchar ) FROM ';
    }else {
        queryString = 'SELECT * FROM ';
    }

    switch (type){
        case 'pool_daily':
            queryString += 'public.pool_summary_day'
            break;
        case 'pool_hourly':
            queryString += 'public.pool_summary_hourly';
            break;
        case 'price_daily':
            queryString += 'public.price_summary_day';
            break;
        case 'pool_hourly':
            queryString += 'public.price_summary_hourly';
            break;
    }

    if (asset !== undefined) {
        queryString += ' WHERE asset = ' + SqlString.escape(asset);
    }else {
        queryString += ' LIMIT 1';
        return  queryString
    }

    if (from !== undefined ) {
        queryString += ' AND bucket >= '+ SqlString.escape(from);
    }

    if (to !== undefined ) {
        queryString += ' AND bucket <= '+ SqlString.escape(to);
    }
    console.log(queryString)
    queryString += 'order by bucket DESC';

    return  queryString
    // SELECT * FROM public.pool_summary_hourly where asset = 'BNB.BNB' LIMIT 1000;

}


const txQuery = function txQuery(address, pool) {

    let queryString =
        'SELECT DISTINCT pool, pool as asset, target, CAST(stakeunit as Bigint) as stakeUnit, CAST(assetamount as Bigint) as assetAmount,' +
        'CAST(targetamount as Bigint) as targetAmount, type, memo, status, EXTRACT(EPOCH FROM time) as time,' +
        ' height ' +
        ' FROM tx_history ' +
        'WHERE ' +
        'address =' + SqlString.escape(address) + ' and pool = ' + SqlString.escape(pool) +' and status = ' + SqlString.escape('Success') + ' AND' +

    ' time > ' +
    'COALESCE(' +
        '(SELECT max(time)' +
    ' FROM tx_history' +
    ' WHERE' +
    ' address = ' + SqlString.escape(address) + 'and pool =' + SqlString.escape(pool) + ' and status = ' + SqlString.escape('Success') +
    ' AND' +
    ' memo LIKE ' + SqlString.escape('%10000') + '),' +
        SqlString.escape('2019-08-25') +
    ') order by time desc;'

    console.log(queryString)

    return  queryString
    // SELECT * FROM public.pool_summary_hourly where asset = 'BNB.BNB' LIMIT 1000;

}

const weeklyQuery = function weeklyQuery(address, pool, isReset) {
    console.log('queryString')
    let queryString =  `
                    SELECT EXTRACT(EPOCH
                               FROM type.date1) AS TIME,
                       TO_CHAR(type.date1, ${SqlString.escape('MM-DD')}) AS DAY,
                       pool.runedepth,
                       pool.assetdepth,
                       pool.poolunits,
                       pool.assetpricerune AS pricerune,
                       pool.busdpricerune AS busdpricerune,
                       Sum(original_stake_unit) OVER(
                                                     ORDER BY type.date1) stakeunit
                FROM
                  (SELECT Time_bucket_gapfill(${SqlString.escape('1 day')}, TIME) AS date1,
                          COALESCE(Sum(CAST (stakeunit AS BIGINT)), 0) AS original_stake_unit
                   FROM
                     (${isReset ? stakingResetTimeSires(address,pool) : stakingNormalTimeSires(address,pool)}) AS TYPE
                LEFT JOIN pool_detail_price_summary_day AS pool ON TYPE.date1 = pool.bucket
                AND pool.asset = ${SqlString.escape(pool)}
                ORDER BY TIME DESC
                LIMIT 14;`;
    console.log(queryString)

    return  queryString
    // SELECT * FROM public.pool_summary_hourly where asset = 'BNB.BNB' LIMIT 1000;

}

const stakingResetTimeSires = function stakingResetTimeSires(address, pool) {
    console.log('queryString')
    let queryString =  `SELECT t.poolunits as stakeunit , u.time
                        FROM staker_pool s
                        LEFT JOIN staker_calculation t ON s.lastid = t.id
                        LEFT join tx_history u on t.tx = u.tx
                        WHERE s.address = ${SqlString.escape(address)}
                        AND s.pool = ${SqlString.escape(pool)}) AS territorysummary
                        WHERE TIME >=  ${SqlString.escape('2020-08-08')}
                        AND TIME < Now()
                        GROUP BY date1
                        ORDER BY date1 DESC
                        `;
    return  queryString
}

const stakingNormalTimeSires = function stakingNormalTimeSires(address, pool) {
    console.log('queryString')
    let queryString =  `SELECT *
                      FROM tx_history
                      WHERE address = ${SqlString.escape(address)}
                        AND pool = ${SqlString.escape(pool)}
                        AND TIME >=
                          (SELECT to_timestamp(firststake) AS TIME
                           FROM staker_pool s
                           LEFT JOIN staker_calculation t ON s.lastid = t.id
                           WHERE s.address = ${SqlString.escape(address)}
                             AND s.pool = ${SqlString.escape(pool)})) AS territorysummary
                       WHERE TIME >= ${SqlString.escape('2020-08-08')}
                         AND TIME < Now()
                       GROUP BY date1
                       ORDER BY date1 DESC
                        `;
    return  queryString
}

const blockPool = function blockPool(heights, asset) {
    console.log('queryString')
    let queryString =
        'SELECT time, asset, balancerune as balance_rune, balanceasset as balance_asset, poolunits as pool_units, status,  height FROM tx_block_pool where ' +
        '(asset = ' + SqlString.escape('BNB.BUSD-BD1') +
        ' OR asset = '+ SqlString.escape(asset) +
        ') AND height IN ('+SqlString.format('?', [heights])+')'

    console.log(queryString)
    return  queryString
}

const blockPoolEach = function blockPool(heights, asset) {
    console.log('queryString')
    let queryString =
        'SELECT time, asset, balancerune as balance_rune, balanceasset as balance_asset, poolunits as pool_units, status,  height FROM tx_block_pool where ' +
        ' asset = '+ SqlString.escape(asset) +
        ' AND height IN ('+SqlString.format('?', [heights])+')'

    console.log(queryString)
    return  queryString
}

const latestTx = function blockPool(address, asset) {
    console.log('queryString')
    let queryString =
        'SELECT EXTRACT(EPOCH FROM time) as time, pool, target, stakeunit, assetamount, targetamount, type, memo, status, height FROM public.tx_history where address = '+
        SqlString.escape(address) + ' and pool = ' +
        SqlString.escape(asset) + 'and status = ' +
        SqlString.escape('Success') + ' order by time DESC limit 1;';
    console.log(queryString)
    return  queryString

}

const stakeUnit = function stakeUnit(address, pools) {
    let queryString = `SELECT s1.pool AS asset,
                            poolunits  :: VARCHAR AS units
                        FROM staker_pool s1
                        LEFT JOIN staker_calculation s2 ON s1.lastid = s2.id
                        WHERE s1.address = ${SqlString.escape(address)}
                        AND s1.pool in (${SqlString.format('?', [pools])});`;
    console.log(queryString)
    return queryString
}


const lastHistorical = function lastHistorical(address, pools) {
    let queryString = ''
    for (let i = 0; i < pools.length; i++){
        const pool = pools[i]
        queryString = queryString + `
           ${eachLastHistroyRecord(address,pool)}
        `;
        if (i < pools.length - 1) {
            queryString = queryString + `UNION`;
        }
    }
    console.log(queryString)
    return queryString
}

const eachLastHistroyRecord  = function eachLastHistroyRecord(address, pool) {
    let queryString = `SELECT s1.pool,
                           CAST (runestake AS DOUBLE PRECISION) :: VARCHAR,
                           CAST (assetstake AS DOUBLE PRECISION) :: VARCHAR,
                           CAST (poolunits AS DOUBLE PRECISION) :: VARCHAR,
                           CAST (assetwithdrawn AS DOUBLE PRECISION) :: VARCHAR,
                           CAST (runewithdrawn AS DOUBLE PRECISION) :: VARCHAR,
                           totalstakedasset :: VARCHAR,
                           totalstakedrune :: VARCHAR,
                           totalstakedusd :: VARCHAR,
                           totalunstakedasset :: VARCHAR,
                           totalunstakedrune :: VARCHAR,
                           totalunstakedusd :: VARCHAR, 
                           firststake,
                           laststake
                    from staker_pool s1
                    left join staker_calculation s2 on s1.lastid = s2.id
                    where s1.address = ${SqlString.escape(address)}
                    AND s1.pool = ${SqlString.escape(pool)};`;
    console.log(queryString)
    return queryString
}

const findDateFirstStake = function findDateFirstStake(address, pool) {
    let queryString = `SELECT extract(epoch
               FROM TIME) AS firstStake
                FROM tx_history
                WHERE id > COALESCE(
                                      (SELECT max(id) AS id
                                       FROM tx_history
                                       WHERE address = ${SqlString.escape(address)}
                                         AND pool = ${SqlString.escape(pool)}
                                         AND status = ${SqlString.escape('Success')}
                                         AND memo LIKE ${SqlString.escape('%10000')}),0)
                  AND address = ${SqlString.escape(address)}
                  AND pool = ${SqlString.escape(pool)}
                  AND status = ${SqlString.escape('Success')}
                ORDER BY height
                LIMIT 1`;
    console.log(queryString)
    return queryString
}



const allStakeLastState = function  allStakeLastState(){
    let queryString = `SELECT t.time as time, sc.address, sc.pool, sc.poolunits as stakeunit, sc.runestake, sc.runewithdrawn, sc.assetstake, sc.assetwithdrawn, sc.firststake as firststakedate, sp.status, 
                        b.balancerune, b.balanceasset, b.poolunits, b.runepricebusd, b.assetpricebusd, b.height
                        
                        FROM "public".staker_pool sp
                        
                        left join staker_calculation sc
                        on sp.lastid = sc.id
                        
                        left join tx_history t
                        on t.tx = sc.tx
                        
                        left join (select * from block_pool_history_sections
                        where id in (SELECT max(id) FROM block_pool_history_sections group by asset) 
                        ) as b
                        on b.asset = sc.pool
                        
                        where sp.status = ${SqlString.escape('Success')} and
                        sc.firststake > 0
                        order by time desc`;
    console.log(queryString)
    return queryString
}

const ranking = function  ranking(){
    let queryString = `SELECT *,
                     RANK () OVER (
                     ORDER BY LPvsHODL DESC) rank_number,
                    EXTRACT(EPOCH
                            FROM computedate)::integer AS lastest
                    FROM member_lp_rank
                    WHERE totalshareusd >= 1000
                      AND days >= 14;`;
    console.log(queryString)
    return queryString
}

const totalLP = function totalSub(){
    let queryString = `SELECT sum(totalshareusd) as totalshare, 
                        (SELECT count(distinct address) FROM member_lp_rank) as usercount 
                        FROM member_lp_rank`;
    console.log(queryString)
    return queryString
}

const averageLP = function average(){
    let queryString = `
            SELECT ceil(AVG(days)) as days, pool, avg(lpvshodl) as lpvshodl
            FROM member_lp_rank
            WHERE totalshareusd >= 1000
            AND days >= 14
            group by pool
            order by pool;
            `;
    console.log(queryString)
    return queryString
}

const stakerLPHistroy =  function (address, pool) {
    let sql = `
        SELECT * FROM staker_calculation
        where id > COALESCE( (SELECT max(id) AS id
                                       FROM staker_calculation
                                       WHERE address = ${SqlString.escape(address)}
                                         AND pool = ${SqlString.escape(pool)}
                                         AND status = ${SqlString.escape('reset')}),0) 
        and address =  ${SqlString.escape(address)}
        and pool =  ${SqlString.escape(pool)}
        order by time asc;
    `
    console.log(sql)
    return sql
}

const getAllActionHistory = function getAllActionHistory(address, pool) {
    const sql = `
                SELECT extract(epoch from date) as time, date, stakeunit :: bigint, height, type, withdrawratio, 
                assetamount :: bigint / POW(10, 8) as assetAmount , targetamount :: bigint / POW(10, 8) as runeAmount
                FROM   tx_history
                WHERE  id > Coalesce((SELECT Max(id)
                                      FROM   tx_history
                                      WHERE
                            address = ${SqlString.escape(address)}
                            AND pool = ${SqlString.escape(pool)}
                            AND status = ${SqlString.escape('Success')}
                            AND withdrawratio = ${SqlString.escape('10000')}
                            AND id < (SELECT id
                                      FROM   tx_history
                                      WHERE
                                address = ${SqlString.escape(address)}
                                AND pool = ${SqlString.escape(pool)}
                                AND status = 'Success'
                                      ORDER  BY height DESC
                                      LIMIT  1)), 0)
                       AND address = ${SqlString.escape(address)}
                       AND pool = ${SqlString.escape(pool)}
                       AND status = ${SqlString.escape('Success')}
                ORDER  BY height asc
    `;
    console.log(sql)
    return sql
}

const getAllAction = () => {
    const sql = `select address, pool, pool.status from staker_pool 
                left join pool on staker_pool.pool = pool.asset
                where staker_pool.status = 'Success' and pool.status = 'Enabled'`;
    console.log(sql)
    return sql
}



module.exports = {
    queryBuilder,
    txQuery,
    weeklyQuery,
    blockPool,
    latestTx,
    stakeUnit,
    lastHistorical,
    allStakeLastState,
    ranking,
    totalLP,
    averageLP,
    stakerLPHistroy,
    getAllActionHistory,
    getAllAction,
    blockPoolEach
}
