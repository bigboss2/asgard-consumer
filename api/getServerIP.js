import ipServer from '../Calculations/serverIPService'

export default async (req, res) => {
  try {
    const ip = await ipServer.getServerIp()
    res.status(200).send({ip_address: ip})
  } catch (error) {
    console.error(error)
    res.status(500).send(error)
  }
}

