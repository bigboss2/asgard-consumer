import {NowRequest, NowResponse} from "@vercel/node";
import {StakerTransaction} from "../model/StakerTransaction";
const axios = require("axios");
import { PrismaClient } from '@prisma/client'
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
import preventExtensions = Reflect.preventExtensions;
const queryBuilder = require('./query.js')
const blockHeight = require('../Calculations/BlockHeight')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

export interface LPAction {
    time: number;
    date: string;
    stakeunit: number;
    height: string;
    type:string;
    withdrawratio: string;
    assetamount: number;
    runeamount: number;
}

export default async (request: NowRequest, response: NowResponse) => {

    const {address} = request.query
    const {pool} = request.query
    const {currentHeight} = request.query
    // const {basisPoint} = request.query

    // console.log(basisPoint)
    // console.log(currentHeight)
    // throw new Error("dfgdfg")

    const lastBlockHeight = currentHeight === undefined ? await blockHeight.lastBlockHeight() : currentHeight
    const fullBasisPoint = 10000
    const blockForFullProtection = 1728000

    //GET action history after the last 100% withdrawal to the latest.
    const actions = await prisma.$queryRaw<LPAction[]>(queryBuilder.getAllActionHistory(address,pool))

    console.log(lastBlockHeight)

    let heights = actions.map(val => val.height);
    heights.push(`${lastBlockHeight}`)

    // let poolData = await prisma.$queryRaw<PoolBlock[]>(queryBuilder.blockPoolEach(heights, pool));
    let poolData = []

    const remainingHeights = filterRemainingHeight(poolData, heights);
    console.log('remainingHeights : ' + remainingHeights)
    if (remainingHeights.length > 0) {
        let newHeights = await blockHeight.getPoolPairAtHeight(remainingHeights, pool, false);
        poolData = poolData.concat(newHeights)

        const validate = filterRemainingHeight(poolData, heights)
        if (validate.length > 0) {
            console.log(newHeights)
            console.log(validate)
            throw new Error("could get all the pool height")
        }
    }

    const poolMap = createPoolMap(poolData);
    const startingAction = actions;
    const endingAction = findLastUnit(actions, lastBlockHeight, fullBasisPoint, poolMap[`${lastBlockHeight}`]);
    let lpProtection = calculateStakingCapital(startingAction, endingAction, poolMap, lastBlockHeight, blockForFullProtection)
    lpProtection['address'] = address
    lpProtection['pool'] = pool

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send({lastBlockHeight, history: actions, poolData, startingAction: startingAction, endingAction,lpProtection})
}

const filterRemainingHeight = (pools: PoolBlock[], heights: string[]) => {
    console.log(`filter remain: ${JSON.stringify(pools)}`)
    const poolMap = createPoolMap(pools);
    let remainingHeights = [];

    heights.forEach(value => {
        if (poolMap[value] === undefined){
            remainingHeights.push(value)
        }
    })

    return remainingHeights
}

const findLastUnit = (startActions: LPAction[], lastBlockHeight: number, basisPoint: number, pool: PoolBlock): LPAction => {
    let total = 0;
    startActions.forEach(value => {
        total = total + value.stakeunit
    })
    const currentTime = new Date()
    console.log(currentTime.getTime())

    total = total * getBasisPoint(basisPoint)
    const share = getR_A(total,pool)

    return {
        time: Math.round(currentTime.getTime() / 1000),
        date: currentTime.toLocaleString(),
        stakeunit: total,
        height: `${lastBlockHeight}`,
        type: 'unstake',
        withdrawratio: `${basisPoint}`,
        assetamount: share.A / 1e8,
        runeamount: share.R / 1e8
    }
}

const calculateStakingCapital = (startActions: LPAction[],
                                 endAction: LPAction,
                                 poolMap: {[height: string]: PoolBlock},
                                 lastHeight: number,
                                 blockForFullProtection: number) => {

    const baseNumber = Math.pow(10, 8); // 1e8
    //Get their startingCapital
    const startingCapital = calculateCombineRO_A0(startActions,poolMap);
    console.log(`-----------`)
    console.log(`Final: R0 ${startingCapital.R0 / baseNumber} : A0 ${startingCapital.A0 / baseNumber}`)

    const R0 = startingCapital.R0;
    const A0 = startingCapital.A0;

    //Get their endingCapital, which is based on their withdrawals when they did it:
    const endingCapital = getR_A(endAction.stakeunit, poolMap[endAction.height]);
    const R1 = Math.abs(endingCapital.R);
    const A1 = Math.abs(endingCapital.A);

    //Get the current Price
    const P1 = R1/A1;

    //Get the depositValue * redeemValue
    const depositValue = R0 + A0 * P1
    const redeemValue = R1 + A1 * P1

    //Get the coverage
    const coverage = redeemValue > depositValue ? 0 : (A0 * R1)/A1 + R0 - 2 * R1

    //then get their protection, which needs to reset daysSinceLastDeposit appropriately
    const daysSinceLastDeposit = getLastStakingDay(getLastStakeAction(startActions),endAction)
    // const protection = 100 * daysSinceLastDeposit * coverage / 10000
    let protection = coverage
    if(daysSinceLastDeposit < 100){
        protection = coverage * (daysSinceLastDeposit / 100)
    }

    const protectionProgress = getProtectionProgress(getLastStakeAction(startActions), lastHeight, blockForFullProtection)
    const newProtection = coverage * protectionProgress / 1e8

    // console.log('date diff = ' + daysSinceLastDeposit / 100)
    // console.log(protection)
    return {
        RO_A0 :{
            R0: R0 / baseNumber,
            A0: A0 / baseNumber
        },
        R1_A1: {
            R1: R1 / baseNumber,
            A1 : A1 / baseNumber
        },
        P1,
        depositValue: depositValue / baseNumber,
        redeemValue: redeemValue / baseNumber ,
        coverage: coverage / baseNumber ,
        daysSinceLastDeposit,
        protection: protection / baseNumber,
        protectionProgress: protectionProgress / baseNumber,
        newProtection: newProtection / baseNumber
    }
}

const getBasisPoint = (ratio: number) => {
    const num = ratio;
    return num / 10000
}

const calculateCombineRO_A0 = (startActions: LPAction[], poolMap: {[height: string]: PoolBlock} ) => {
    const baseNumber = Math.pow(10, 8); // 1e8
    let R0 = 0;
    let A0 = 0
    startActions.forEach(value => {
        console.log(value)
        if (value.type === 'unstake'){
            console.log(`Before Unstake: R0 ${R0 / baseNumber } : A0 ${A0 / baseNumber}`)
            R0 = R0 - (R0 * getBasisPoint(parseInt(value.withdrawratio)));
            A0 = A0 - (A0 * getBasisPoint(parseInt(value.withdrawratio)));
            console.log(`After Unstake: R0 ${R0 / baseNumber} : A0 ${A0 / baseNumber}`)
        }else {
            const pool = poolMap[value.height]
            const R_A = getR_A(value.stakeunit, pool)
            console.log(R_A)
            console.log(`Before Stake: R0 ${R0 / baseNumber } : A0 ${A0 / baseNumber}`)
            R0 = R0 + R_A.R
            A0 = A0 + R_A.A
            console.log(`After Stake: R0 ${R0 / baseNumber} : A0 ${A0 / baseNumber}`)
        }
    })
    return {
        R0,A0
    }
}

const getR_A = (lpUnit: number, pool: PoolBlock) => {
    const R = (lpUnit * pool.balance_rune) / pool.pool_units
    const A = (lpUnit * pool.balance_asset) / pool.pool_units
    return {R , A}
}

const getLastStakingDay = (lastAction: LPAction, endAction: LPAction) => {
    console.log('----- last getLastStakingDay ------: ' + JSON.stringify(lastAction))
    console.log('----- last endAction ------: ' + JSON.stringify(endAction))
   const diff =  (endAction.time * 1000) - (lastAction.time * 1000)
    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
   return diffDays
}

const getProtectionProgress = (lastAction: LPAction, currentHeight: number, blocksForFullProtection: number) => {
    console.log('----- last getProtectionProgress ------: ' + JSON.stringify(lastAction))
    const heightLastAdded = parseInt(lastAction.height)
    const protectionProgress = Math.min(((currentHeight - heightLastAdded) * 1e8 / blocksForFullProtection), 1e8)
    console.log(`last height: ${currentHeight} : lastHeight: ${lastAction.height}`)
    return protectionProgress
}

const getLastStakeAction = (actions: LPAction[]) => {
    const lastStakeAction = actions.filter(value => value.type === 'stake')
        .sort((a, b) => (parseInt(a.height) > parseInt(b.height) ? -1 : 1));
   // console.log('----- last stake ------')
  //  console.log(lastStakeAction)
    console.log('----- last stake ------: ' + JSON.stringify(lastStakeAction[0]))
    return lastStakeAction[0]
}

function createPoolMap(poolData: PoolBlock[]){
    let poolMap: { [height: string]: PoolBlock } = {};
    for (const value of poolData){
        poolMap[value.height] = value
    }
    return poolMap
}


