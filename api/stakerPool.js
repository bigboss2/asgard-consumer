//@ts-check
import { PrismaClient } from '@prisma/client'
const query = require('./query.js')
const axios = require("axios");

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
  ],
})
prisma.$on('query', e => {
  e.query, console.log(e)
})

export default async (req, res) => {
  try {
    const address = req.query.address;
    const pools = req.query.pools;

    var paths = []
    pools.split(',').forEach(value => {
        console.log(value)
     const path = `https://chaosnet-midgard.bepswap.com/v1/stakers/${address}/pools?asset=${value}`;
        console.log(path)
     paths.push(axios.get(path))
    })

    const result = await axios.all(paths)
    var poolResult = [];
    result.forEach(value => {
      const data = value.data;
      console.log(data);
      if (data !== undefined){
        poolResult.push(data[0])
      }
    })
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.status(200).json(poolResult)
  } catch (error) {
    console.error(error)
    res.status(500).json(error)
  }
}
