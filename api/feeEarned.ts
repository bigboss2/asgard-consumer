import {NowRequest, NowResponse} from "@vercel/node";
import {StakerTransaction} from "../model/StakerTransaction";
const axios = require("axios");
import { PrismaClient } from '@prisma/client'
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
const queryBuilder = require('./query.js')
const blockHeight = require('../Calculations/BlockHeight')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

const BUSD = 'BNB.BUSD-BD1';

const history = `
    
`


export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    // const body  = request.body
    
    const address = query.address
    const asset = query.pool
    const baseNumber = Math.pow(10, 8); // 1e8
    const lastBlock = await blockHeight.lastBlockHeight()
    // const asset = 'BNB.BNB'

    let data  = await prisma.$queryRaw(queryBuilder.stakerLPHistroy(
        address,
        asset))
    let height = data.map(val => `${val.height}`)
    height = height.concat(`${lastBlock}`)
    console.log(height)
   // let poolData = await getAllPoolData(asset, 1, height)
    let poolData = await blockHeight.getPoolPairAtHeight(height, asset)
    console.log(poolData)
    let poolMap = createPoolMap(poolData)

    const last = data[data.length - 1]
    console.log(`last : ${data.length} + ${JSON.stringify(last)}`)
  data.push({height:`${lastBlock}`, poolunits : last.poolunits})
    console.log('------combined--------')
    console.log('sdfsf' + data)

    let snapshots: Position[] = []

    for (let i = 0; i < data.length; i++) {
        const each = data[i];
        console.log(`height at each: ${each}`)
        const pool = poolMap[0][each.height]
        const busd = poolMap[1][each.height]
        const price = getPrice(pool, busd)
        const stakerShare = doCalculation(pool, each.poolunits)
        const pos: Position = {
            height: each.height,
            pair: each.pool,
            liquidityTokenBalance: each.poolunits / baseNumber,
            liquidityTokenTotalSupply: pool.pool_units / baseNumber,
            reserve0: pool.balance_rune / baseNumber,
            reserve1: pool.balance_asset / baseNumber,
            reserveUSD: ((pool.balance_rune * price.runePriceInBUSD ) + (pool.balance_asset * price.assetPriceInBUSD))/baseNumber,
            token0PriceUSD: price.runePriceInBUSD,
            token1PriceUSD: price.assetPriceInBUSD
        }
        snapshots.push(pos)
    }

    let hodlReturn = 0
    let netReturn = 0
    let uniswapReturn = 0
    let fees = 0
    let impLoss = 0
    let percent = 0

    for (let i = 0; i < snapshots.length - 1; i++) {
        // get positions at both bounds of the window
        let positionT0 = snapshots[i]
        let positionT1 = snapshots[i+1]

        let results = getMetricsForPositionWindow(positionT0, positionT1)
     //   let newresult = calculatePLBreakdown(positionT0, positionT1)
        hodlReturn = hodlReturn + results.hodleReturn
        netReturn = netReturn + results.netReturn
        uniswapReturn = uniswapReturn + results.uniswapReturn
        fees = fees + results.fees
        impLoss = impLoss + results.impLoss
        percent = percent + results.percentage
    }


    const lastPool = poolMap[0][`${lastBlock}`]
    const poolShare = doCalculation(lastPool, last.poolunits);
    const lastPrice = getPrice(lastPool, poolMap[1][`${lastBlock}`])

    const freerune = (fees / 2) / lastPrice.runePriceInBUSD
    const feeAsset = (fees / 2) / lastPrice.assetPriceInBUSD


    // const newfreerune = (feeNEw / 2) / lastPrice.runePriceInBUSD
    // const newfeeAsset = (feeNEw / 2) / lastPrice.assetPriceInBUSD

    // const feeINrune =
    console.log(`last state`)
    console.log(lastBlock)
    console.log(last)
    console.log(poolShare)
    console.log(lastPrice)

    const net = {
        fee: {
            usd: fees,
            rune: freerune,
            asset: feeAsset
        },
        impLoss: {
            usd: impLoss,
            percent: percent
        },
        asset
    }


    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send(net)
}


/**
 * Core algorithm for calculating retursn within one time window.
 * @param positionT0 // users liquidity info and token rates at beginning of window
 * @param positionT1 // '' at the end of the window
 */

interface ReturnMetrics {
    hodleReturn: number // difference in asset values t0 -> t1 with t0 deposit amounts
    netReturn: number // net return from t0 -> t1
    uniswapReturn: number // netReturn - hodlReturn
    impLoss: number
    fees: number,
    percentage: number
}

// used to calculate returns within a given window bounded by two positions
interface Position {
    pair: any
    liquidityTokenBalance: number
    liquidityTokenTotalSupply: number
    reserve0: number
    reserve1: number
    reserveUSD: number
    token0PriceUSD: number
    token1PriceUSD: number
    height?: string
}


export function getMetricsForPositionWindow(positionT0: Position, positionT1: Position): ReturnMetrics {

    // calculate ownership at ends of window, for end of window we need original LP token balance / new total supply
    const t0Ownership = positionT0.liquidityTokenBalance / positionT0.liquidityTokenTotalSupply
    const t1Ownership = positionT0.liquidityTokenBalance / positionT1.liquidityTokenTotalSupply

    // get starting amounts of token0 and token1 deposited by LP
    const token0_amount_t0 = t0Ownership * positionT0.reserve0
    const token1_amount_t0 = t0Ownership * positionT0.reserve1

    // get current token values
    const token0_amount_t1 = t1Ownership * positionT1.reserve0
    const token1_amount_t1 = t1Ownership * positionT1.reserve1

    // calculate squares to find imp loss and fee differences
    const sqrK_t0 = Math.sqrt(token0_amount_t0 * token1_amount_t0)
    // eslint-disable-next-line eqeqeq
    const priceRatioT1 = positionT1.token0PriceUSD != 0 ? positionT1.token1PriceUSD / positionT1.token0PriceUSD : 0

    const token0_amount_no_fees = positionT1.token1PriceUSD && priceRatioT1 ? sqrK_t0 * Math.sqrt(priceRatioT1) : 0
    const token1_amount_no_fees =
        Number(positionT1.token1PriceUSD) && priceRatioT1 ? sqrK_t0 / Math.sqrt(priceRatioT1) : 0
    const no_fees_usd =
        token0_amount_no_fees * positionT1.token0PriceUSD + token1_amount_no_fees * positionT1.token1PriceUSD

    const difference_fees_token0 = token0_amount_t1 - token0_amount_no_fees
    const difference_fees_token1 = token1_amount_t1 - token1_amount_no_fees
    const difference_fees_usd =
        difference_fees_token0 * positionT1.token0PriceUSD + difference_fees_token1 * positionT1.token1PriceUSD

    // calculate USD value at t0 and t1 using initial token deposit amounts for asset return
    const assetValueT0 = token0_amount_t0 * positionT0.token0PriceUSD + token1_amount_t0 * positionT0.token1PriceUSD
    const assetValueT1 = token0_amount_t0 * positionT1.token0PriceUSD + token1_amount_t0 * positionT1.token1PriceUSD

    const imp_loss_usd = no_fees_usd - assetValueT1
    const uniswap_return = difference_fees_usd + imp_loss_usd

    // get net value change for combined data
    const netValueT0 = t0Ownership * positionT0.reserveUSD
    const netValueT1 = t1Ownership * positionT1.reserveUSD

    return {
        hodleReturn: assetValueT1 - assetValueT0,
        netReturn: netValueT1 - netValueT0,
        uniswapReturn: uniswap_return,
        impLoss: imp_loss_usd,
        fees: difference_fees_usd,
        percentage:( Math.abs(imp_loss_usd) / ((positionT0.token0PriceUSD * token0_amount_t1) + (positionT0.token1PriceUSD * token1_amount_t0))),
    }
}




function createPoolMap(poolData: PoolBlock[]){
    let poolMap: { [height: string]: PoolBlock } = {};
    let poolBUSDMap: { [height: string]: PoolBlock } = {};
    for (const value of poolData){
      //  console.log(value);
        if (value.asset === BUSD) {
            poolBUSDMap[value.height] = value
        }else {
            poolMap[value.height] = value
        }
    }
    return [poolMap, poolBUSDMap]
}

function getPrice(ASSETPOOL: PoolBlock, BUSD: PoolBlock): Price {
    console.log('asset pool ' + JSON.stringify(ASSETPOOL))
    console.log('BUSD pool ' + JSON.stringify(BUSD))
    if (ASSETPOOL === undefined && BUSD === undefined) {
        return
    }

    //  What is the price of the asset in RUNE?
    let BUSD_RUNEPRICE = BUSD.balance_rune / BUSD.balance_asset
    let BNB_RUNEPRICE = ASSETPOOL.balance_rune / ASSETPOOL.balance_asset
    console.log(`AT HEIGHT: ${ASSETPOOL.height}`)

    console.log(`BUSD PRICE IN RUNE: ${BUSD_RUNEPRICE.toFixed(4)}`)
    console.log(`BNB PRICE IN RUNE: ${BNB_RUNEPRICE.toFixed(4)}`)

    // What is the price of RUNE in the asset?
    let BUSD_ASSETPRICE = BUSD.balance_asset / BUSD.balance_rune
    let BNB_ASSETPRICE =   ASSETPOOL.balance_asset / ASSETPOOL.balance_rune

    console.log(`RUNE PRICE IN BUSD: ${BUSD_ASSETPRICE.toFixed(4)}`)
    console.log(`RUNE PRICE IN BNB: ${BNB_ASSETPRICE.toFixed(4)}`)

    // What is the price of one asset in another?
    let BNB_BUSDPRICE =  BNB_RUNEPRICE / BUSD_RUNEPRICE

    console.log(`BNB PRICE IN BUSD: ${BNB_BUSDPRICE.toFixed(4)}`)

    return {
        asset: ASSETPOOL.asset,
        assetPriceInBUSD:BNB_BUSDPRICE,
        assetPriceInRune: BNB_RUNEPRICE,
        runePriceInAsset: BNB_ASSETPRICE,
        runePriceInBUSD:BUSD_ASSETPRICE,
        busdPriceInRune: BUSD_RUNEPRICE
    }

}

function doCalculation(ASSETPOOL: PoolBlock, stakeunit: number): StakerPoolShare {
    console.log(stakeunit)
    const baseNumber = Math.pow(10, 8); // 1e8
    if (ASSETPOOL === undefined) {
        return
    }

    let stakeUnitsBN = stakeunit
    let runeDepthBN = ASSETPOOL.balance_rune
    let assetDepthBN = ASSETPOOL.balance_asset
    let poolUnit = ASSETPOOL.pool_units

    let runeShare = (runeDepthBN * stakeUnitsBN)/poolUnit
    let assetShare = (assetDepthBN * stakeUnitsBN)/poolUnit



    return {
        runeShare: runeShare,
        assetShare: assetShare,
        asset: ASSETPOOL.asset
    }
}



const getTranscationData = async (ASSET, ADDRESS) => {
    try {
        const TXS_URL = `https://asgard-consumer.vercel.app/api/staker?address=${ADDRESS}&pool=${ASSET}`;
        const TXS = await axios.get(TXS_URL)
        return TXS.data.data
    } catch(e){
        //    console.log(e)
        return;
    }
}

const getAllPoolData = async(ASSET, TOTAL: number, HEIGHT: string[]) => {

    let path = []

  //  console.log('calling count: ' + TOTAL.length + ' total: ' + TOTAL + ' REMain : ' + TOTAL.length)

    const  limit = 10;

    const pageNo = Math.ceil(TOTAL/limit);
    let results  = []
    var i;
    for (i = 0; i < pageNo; i++) {
        const heights = HEIGHT.slice(i * limit,(i * limit) + limit)
        console.log('calling height ' + heights)
        for (const height of heights){
            if (ASSET !== BUSD) {
                const BUSD_URL = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=BNB.BUSD-BD1&height=${height}`
                path.push(axios.get(BUSD_URL))
            }
            const POOL_URL = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=${ASSET}&height=${height}`
            path.push(axios.get(POOL_URL))

        }
        const response = await axios.all(path)
        //   console.log(path)
        for (const data of response) {
            results.push(data.data)
        }
    }

    console.log('length ' + results.length + 'vs  total: ' + TOTAL)
  //  console.log(results)
    return results
}


const stall = async(ms=1500) => {await new Promise(resolve => setTimeout(resolve, ms));
}


