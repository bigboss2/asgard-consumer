//@ts-check
import { PrismaClient } from '@prisma/client'
const query = require('./query.js')

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
  ],
})
prisma.$on('query', e => {
  e.query, console.log(e)
})

export default async (req, res) => {
  try {
    const users = await prisma.$queryRaw(query.queryBuilder(req,'pool_daily','pool') + ';');
    res.status(200).json({
      count: users.length === undefined ? 0 : users.length,
      data: users})
  } catch (error) {
    console.error(error)
    res.status(500).json(error)
  }
}


// const users = await prisma.pool_summary_day.findMany({
//   where: {
//     AND: [
//       {
//         asset: {
//           equals: 'BNB.BNB',
//         },
//       },
//       {
//         bucket: {
//           gte: new Date('2020-10-02T00:00:00.000Z'),
//         },
//       },
//     ],
//   },
//   orderBy: {
//     bucket: 'desc',
//   },
// })
