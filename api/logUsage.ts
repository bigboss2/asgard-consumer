import {NowRequest, NowResponse} from "@vercel/node";
import {StakersAssetData} from "../model/stakersAssetData";
import {PrismaClient} from "@prisma/client";
import {StakerTransaction} from "../model/StakerTransaction";
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
const axios = require("axios");
const queryBuilder = require('./query.js')
 const PoolShare = require('../Calculations/PoolShareCalculator')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    // @ts-ignore
    const useType = query.type
    const agent = `${query.agent}`
    // console.log(`${process.env.EXTERNAL_URL}/api/logUsage?agent=${agent}&type=${useType}&key=${process.env.EXTERNAL_SECRET}`)
    const log = await axios.get(`${process.env.EXTERNAL_URL}/api/logUsage?agent=${agent}&type=${useType}&key=${process.env.EXTERNAL_SECRET}`).catch(error =>{
        console.log(error.toString())
    })
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send({status: 'logged'})
}
