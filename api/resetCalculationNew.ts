import {NowRequest, NowResponse} from "@vercel/node";
import {StakersAssetData} from "../model/stakersAssetData";
import {PrismaClient} from "@prisma/client";
import {StakerTransaction} from "../model/StakerTransaction";
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
import {PoolStakeAmount, StakePoolSummary} from "../model/StakerSummary";
import {StakerHistoryPool} from "../model/StakerHistoryPool";
const axios = require("axios");
const queryBuilder = require('./query.js')
 const PoolShare = require('../Calculations/PoolShareCalculator')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    const pool = query.pools
    let address = query.address
    const isDev = query.isDev

    //get user stakeunit
    // const result  = await axios.get(`https://chaosnet-midgard.bepswap.com/v1/stakers/${address}/pools?asset=${pool}`)
    // console.log(`https://chaosnet-midgard.bepswap.com/v1/stakers/${address}/pools?asset=${pool}`)
    const result = await prisma.$queryRaw(queryBuilder.stakeUnit(address,[pool]))
    console.log(result)
    const stakerAsset: StakersAssetData = result[0]
    console.log(stakerAsset)

    const txs = await prisma.$queryRaw(queryBuilder.latestTx(address,pool));
    const stakerLatestTx: StakerTransaction = txs[0]
    console.log(stakerLatestTx)
    stakerLatestTx.stakeunit = parseInt(stakerAsset.units)
    console.log(stakerLatestTx)
    const blockPools: PoolBlock[] = await prisma.$queryRaw(queryBuilder.blockPool(stakerLatestTx.height, pool));
    let assetPool:PoolBlock = blockPools.filter(value => value.asset === pool)[0];
    let busdPool:PoolBlock = blockPools.filter(value => value.asset === 'BNB.BUSD-BD1')[0];

    if (assetPool === undefined) {
        console.log(`calling ${pool} for height ` + stakerLatestTx.height);
        const asset_url = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=${pool}&height=${stakerLatestTx.height}`
        const result = await axios.get(asset_url);
        assetPool = result.data
    }

    if (busdPool === undefined) {
        console.log("calling BUSD for height " + stakerLatestTx.height);
        const busd_url = `https://asgard-consumer.vercel.app/api/poolheightdetail?pool=BNB.BUSD-BD1&height=${stakerLatestTx.height}`
        const result = await axios.get(busd_url);
        busdPool = result.data
    }

    const poolShare: StakerPoolShare = PoolShare.poolShare(assetPool, stakerLatestTx);
    const price: Price = PoolShare.getPrice(assetPool, busdPool)

    stakerLatestTx.targetamount = poolShare.runeShare
    stakerLatestTx.assetamount = poolShare.assetShare
    stakerLatestTx.type = 'stake'
    stakerLatestTx.asset = `${pool}`
    stakerLatestTx.pool = `${pool}`
  //
  // //  console.log(stakerAsset)
  //   stakerLatestTx.stakeunit = parseInt(stakerAsset.units)

    const stakerTxDetail: StakerTxDetail = {
        // poolHistory: value.asset === BUSD ?  poolBUSDMap[value.height]:  poolMap[value.height],
        // poolShare: poolShare,
        price: price,
        stakeTransaction: stakerLatestTx
    }

    const totalRuneStake = stakerLatestTx.targetamount + (stakerLatestTx.assetamount * price.assetPriceInRune);
    const totalAssetStake = stakerLatestTx.assetamount + (stakerLatestTx.targetamount * price.runePriceInAsset);
    const totalUSD = (stakerLatestTx.assetamount * price.assetPriceInBUSD) + (stakerLatestTx.targetamount * price.runePriceInBUSD)

    const summary: StakerHistoryPool = {
        pool: stakerLatestTx.asset,
        runestake: `${stakerLatestTx.targetamount}`,
        assetstake: `${stakerLatestTx.assetamount}`,
        poolunits: `${stakerLatestTx.stakeunit}`,
        assetwithdrawn: '0',
        runewithdrawn: '0',
        totalstakedasset: `${totalAssetStake}`,
        totalstakedrune: `${totalRuneStake}`,
        totalstakedusd: `${totalUSD}`,
        totalunstakedasset: '0',
        totalunstakedrune: '0',
        totalunstakedusd: '0',
        firststake: stakerLatestTx.time

    }
    const data: StakerTxState = {
        count: 1,
        pool: `${pool}`,
        txDetail: [stakerTxDetail]
    }

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send(summary)
}

