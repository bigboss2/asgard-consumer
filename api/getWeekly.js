//@ts-check
import { PrismaClient } from '@prisma/client'
const query = require('./query.js')
const SqlString = require('sqlstring');

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
  ],
})
prisma.$on('query', e => {
  e.query, console.log(e)
})

export default async (req, res) => {

  const request = req.query
  const address = request.address
  const pool = request.pool
  const reset = request.reset

  console.log(address)
  console.log(pool)
  console.log(reset)
  // console.log(req.get('user-agent'));
  console.log(req.headers.origin);

  try {
    const users = await prisma.$queryRaw(query.weeklyQuery(address,pool, reset === undefined ? false : true));
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.status(200).json({
      pool: pool,
      data: users})
  } catch (error) {
    console.error(error)
    res.status(500).json(error)
  }
}

