import {NowRequest, NowResponse} from "@vercel/node";
import {StakersAssetData} from "../model/stakersAssetData";
import {PrismaClient} from "@prisma/client";
import {StakerTransaction} from "../model/StakerTransaction";
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
const axios = require("axios");
const queryBuilder = require('./query.js')
 const PoolShare = require('../Calculations/PoolShareCalculator')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    const pools = `${query.pools}`
    let address = query.address
    console.log(request.headers.origin);
    // const log = axios.get(process.env.EXTERNAL_URL + '/api/logUsage?type=normal&key='+process.env.EXTERNAL_SECRET).then(value => {
    //     console.log(value.data)
    // }).catch(error =>{
    //     console.log(error.toString())
    // })

    // console.log(process.env.EXTERNAL_URL + '/api/logUsage?type=normal');

    const historical = await prisma.$queryRaw(queryBuilder.lastHistorical(address, pools.split(','))).catch(onerror => {
        console.log(onerror)
        response.status(500).send(data)
        return
    })
    // const result = await Promise.all([log, historical])
    // console.log(result[1])
    const data = historical[0]
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send(data)
}
