import {NowRequest, NowResponse} from "@vercel/node";
import {StakerTransaction} from "../model/StakerTransaction";
const axios = require("axios");
import { PrismaClient } from '@prisma/client'
import {PoolBlock, Price, StakerPoolShare, StakerTxDetail, StakerTxState} from "../model/StakerState";
import preventExtensions = Reflect.preventExtensions;
const queryBuilder = require('./query.js')
const blockHeight = require('../Calculations/BlockHeight')
const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

export default async (request: NowRequest, response: NowResponse) => {
    const lastBlockHeight = await blockHeight.lastBlockHeight()
    const actions = await prisma.$queryRaw(queryBuilder.getAllAction())
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send({lastBlockHeight, actions})
}
