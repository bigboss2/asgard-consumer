import {NowRequest, NowResponse} from "@vercel/node";
import {StakerTransaction} from "../model/StakerTransaction";
import {PoolBlock} from "../model/BlockModel";
import {PoolDetail} from "../model/poolDetail";
const axios = require("axios");
const poolService = require('../Calculations/BlockHeight')
export default async (request: NowRequest, response: NowResponse) => {

    const query = request.query
    const ASSET = `${query.pool}`

    const result:PoolBlock[] = await poolService.getAllLastestPool()
    console.log(result)
    const lastest = result.filter(value => {
       return ASSET.search(value.asset) !== -1
    }).map(convertPoolBlock)
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.status(200).send(lastest)
}

const convertPoolBlock = (poolBlock: PoolBlock) =>{
    const price =  poolBlock.balance_rune / poolBlock.balance_asset
    const poolDetail: PoolDetail = {
        asset: poolBlock.asset,
        assetDepth: `${poolBlock.balance_asset}`,
        runeDepth: `${poolBlock.balance_rune}`,
        poolUnits: `${poolBlock.pool_units}`,
        price: `${price}`
    }
    return poolDetail
}


const stall = async(ms=1500) => {await new Promise(resolve => setTimeout(resolve, ms));
}


