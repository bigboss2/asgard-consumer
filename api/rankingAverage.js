//@ts-check
import { PrismaClient } from '@prisma/client'
import {PoolBlock} from "../model/StakerState";
const query = require('./query.js')
const axios = require("axios");
// cosst qu

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
  ],
})
prisma.$on('query', e => {
  e.query, console.log(e)
})

export default async (req, res) => {
  try {
    // const address = req.query.address;
    // const pools = req.query.pools;

    let poolData = await prisma.$queryRaw(query.averageLP());
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.status(200).json(poolData)
  } catch (error) {
    console.error(error)
    res.status(500).json(error)
  }
}
