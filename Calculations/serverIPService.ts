import { PrismaClient } from '@prisma/client'
// or const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()
import {ips} from "../model/ips"

const getServerIp = async() => {
    try {
        const ips = await prisma.$queryRaw<ips[]>('SELECT address FROM public.server_ips order by time limit 10;');
        // @ts-ignore
        console.log(ips)
        return ips[(Math.random()*ips.length)|0].address
    } catch(e){
        console.log(e)
        return;
    }
}

module.exports = {
    getServerIp
}
