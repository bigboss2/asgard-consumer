import {PoolBlock, Price, StakerPoolShare} from "../model/StakerState";
import {StakerTransaction} from "../model/StakerTransaction";

const poolShare = function poolShare(ASSETPOOL: PoolBlock, staker: StakerTransaction): StakerPoolShare {
    const baseNumber = Math.pow(10, 8); // 1e8
    if (ASSETPOOL === undefined) {
        return
    }

    let stakeUnitsBN = staker.stakeunit
    let runeDepthBN = ASSETPOOL.balance_rune
    let assetDepthBN = ASSETPOOL.balance_asset
    let poolUnit = ASSETPOOL.pool_units

    let runeShare = (runeDepthBN * stakeUnitsBN)/poolUnit
    let assetShare = (assetDepthBN * stakeUnitsBN)/poolUnit

    console.log(`runeShare: ${(runeShare / baseNumber).toFixed(4)}`)
    console.log(`assetShare: ${(assetShare / baseNumber).toFixed(4)}`)

    return {
        runeShare: runeShare,
        assetShare: assetShare,
        asset: ASSETPOOL.asset
    }
}

const getPrice = function getPrice(ASSETPOOL: PoolBlock, BUSD: PoolBlock): Price {
    console.log('asset pool ' + JSON.stringify(ASSETPOOL))
    console.log('BUSD pool ' + JSON.stringify(BUSD))
    if (ASSETPOOL === undefined && BUSD === undefined) {
        return
    }

    //  What is the price of the asset in RUNE?
    let BUSD_RUNEPRICE = BUSD.balance_rune / BUSD.balance_asset
    let BNB_RUNEPRICE = ASSETPOOL.balance_rune / ASSETPOOL.balance_asset
    console.log(`AT HEIGHT: ${ASSETPOOL.height}`)

    console.log(`BUSD PRICE IN RUNE: ${BUSD_RUNEPRICE.toFixed(4)}`)
    console.log(`BNB PRICE IN RUNE: ${BNB_RUNEPRICE.toFixed(4)}`)

    // What is the price of RUNE in the asset?
    let BUSD_ASSETPRICE = BUSD.balance_asset / BUSD.balance_rune
    let BNB_ASSETPRICE =   ASSETPOOL.balance_asset / ASSETPOOL.balance_rune

    console.log(`RUNE PRICE IN BUSD: ${BUSD_ASSETPRICE.toFixed(4)}`)
    console.log(`RUNE PRICE IN BNB: ${BNB_ASSETPRICE.toFixed(4)}`)

    // What is the price of one asset in another?
    let BNB_BUSDPRICE =  BNB_RUNEPRICE / BUSD_RUNEPRICE

    console.log(`BNB PRICE IN BUSD: ${BNB_BUSDPRICE.toFixed(4)}`)

    return {
        asset: ASSETPOOL.asset,
        assetPriceInBUSD:BNB_BUSDPRICE,
        assetPriceInRune: BNB_RUNEPRICE,
        runePriceInAsset: BNB_ASSETPRICE,
        runePriceInBUSD:BUSD_ASSETPRICE,
        busdPriceInRune: BUSD_RUNEPRICE
    }

}

module.exports = {
    poolShare,
    getPrice
}
