import {PrismaClient} from "@prisma/client";
import validate = WebAssembly.validate;
import {PoolBlock, Price} from "../model/StakerState";
const axios = require("axios");
const queryBuilder = require('../api/query.js')
const BUSD = 'BNB.BUSD-BD1';
const blockHeight = require('../Calculations/BlockHeight')

const prisma = new PrismaClient({
    log: [
        {
            emit: 'event',
            level: 'query',
        },
    ],
})
prisma.$on('query', e => {
    e.query, console.log(e)
})

const getLPHistory = async (address: string, pool: string) => {
    console.log('querying LP history: ')
    let data = await prisma.$queryRaw<UnitHistory[]>(queryBuilder.stakerLPHistroy(address, pool))
    const heights = data.map(value =>value.height)
    const poolBlocks = await getAllPoolBlocks(pool, heights)
    const snapShots = getPositions(data, poolBlocks, pool)
    console.log(data)
    return snapShots
}

function getPositions(data: UnitHistory[], poolBlocks: PoolBlock[], pool: string ){
    const poolMap = createPoolMap(poolBlocks, pool)
    console.log(poolMap)
    let snapshots: Position[] = []
    const baseNumber = Math.pow(10, 8); // 1e8

    for (let i = 0; i < data.length; i++) {
        const each = data[i];
        console.log(`height at each: ${each.height} : ${JSON.stringify(poolMap.poolMap[each.height])}`)
        const pool = poolMap.poolMap[each.height]
        const busd = poolMap.poolBUSDMap[each.height]
        const price = getPrice(pool, busd)
        const pos: Position = {
            height: `${each.height}`,
            pair: each.pool,
            liquidityTokenBalance: each.poolunits / baseNumber,
            liquidityTokenTotalSupply: pool.pool_units / baseNumber,
            reserve0: pool.balance_rune / baseNumber,
            reserve1: pool.balance_asset / baseNumber,
            reserveUSD: ((pool.balance_rune * price.runePriceInBUSD ) + (pool.balance_asset * price.assetPriceInBUSD))/baseNumber,
            token0PriceUSD: price.runePriceInBUSD,
            token1PriceUSD: price.assetPriceInBUSD
        }
        snapshots.push(pos)
    }
    return snapshots;
}

function getPrice(ASSETPOOL: PoolBlock, BUSD: PoolBlock): Price {
    console.log('asset pool ' + JSON.stringify(ASSETPOOL))
    console.log('BUSD pool ' + JSON.stringify(BUSD))
    console.log('height ' + BUSD.height)
    if (ASSETPOOL === undefined && BUSD === undefined) {
        return
    }

    //  What is the price of the asset in RUNE?
    let BUSD_RUNEPRICE = BUSD.balance_rune / BUSD.balance_asset
    let BNB_RUNEPRICE = ASSETPOOL.balance_rune / ASSETPOOL.balance_asset
    console.log(`AT HEIGHT: ${ASSETPOOL.height}`)

    console.log(`BUSD PRICE IN RUNE: ${BUSD_RUNEPRICE.toFixed(4)}`)
    console.log(`BNB PRICE IN RUNE: ${BNB_RUNEPRICE.toFixed(4)}`)

    // What is the price of RUNE in the asset?
    let BUSD_ASSETPRICE = BUSD.balance_asset / BUSD.balance_rune
    let BNB_ASSETPRICE =   ASSETPOOL.balance_asset / ASSETPOOL.balance_rune

    console.log(`RUNE PRICE IN BUSD: ${BUSD_ASSETPRICE.toFixed(4)}`)
    console.log(`RUNE PRICE IN BNB: ${BNB_ASSETPRICE.toFixed(4)}`)

    // What is the price of one asset in another?
    let BNB_BUSDPRICE =  BNB_RUNEPRICE / BUSD_RUNEPRICE

    console.log(`BNB PRICE IN BUSD: ${BNB_BUSDPRICE.toFixed(4)}`)

    return {
        asset: ASSETPOOL.asset,
        assetPriceInBUSD:BNB_BUSDPRICE,
        assetPriceInRune: BNB_RUNEPRICE,
        runePriceInAsset: BNB_ASSETPRICE,
        runePriceInBUSD:BUSD_ASSETPRICE,
        busdPriceInRune: BUSD_RUNEPRICE
    }

}

const getAllPoolBlocks = async (pool: string, heights: number[]) => {
    const blockPools = await prisma.$queryRaw<PoolBlock[]>(queryBuilder.blockPool(heights,pool))
    const remains = findRemainingPool(heights, blockPools, pool);
    let extraPools = await blockHeight.getPoolPairAtHeight(remains, pool)
    return blockPools.concat(extraPools)
}


function findRemainingPool(heights: number[], existingPools: PoolBlock[], pool: string){
   const remains = heights.filter(value => !isExist(pool,value, existingPools));
   return remains
}

function isExist(pool: string, height: number, existingPools: PoolBlock[]): boolean {
   return  existingPools.filter(value => (value.asset === pool && value.height === height) ||
        (value.asset === BUSD && value.height === height)).length > 0
}


function createPoolMap(poolData: PoolBlock[], pool: string){
    let poolMap: { [height: string]: PoolBlock } = {};
    let poolBUSDMap: { [height: string]: PoolBlock } = {};
    for (const value of poolData){
        //  console.log(value);
        if (value.asset === BUSD) {
            poolBUSDMap[value.height] = value
            if (pool === BUSD){
                poolMap[value.height] = value
            }
        }else {
            poolMap[value.height] = value
        }
    }
    return {poolMap, poolBUSDMap}
}


module.exports = {
    getLPHistory
}

export interface UnitHistory {
    pool: string;
    poolunits: number;
    height: number
}

// used to calculate returns within a given window bounded by two positions
interface Position {
    pair: any
    liquidityTokenBalance: number
    liquidityTokenTotalSupply: number
    reserve0: number
    reserve1: number
    reserveUSD: number
    token0PriceUSD: number
    token1PriceUSD: number
    height?: string
}
