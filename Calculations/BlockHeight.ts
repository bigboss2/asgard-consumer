import {BlockHeight, PoolBlock, PoolPrice} from "../model/BlockModel";
import {LastBlock} from "../model/lastBlock";
import {BlockData} from "../model/StakerState";
const axios = require("axios");
const BUSD = 'BNB.BUSD-BD1';
const getAllLastestPool = async() => {
    try {
        const ip = await getNodeIp()
    //    const HEIGHT = await lastBlockHeight()
        console.log(ip)
        console.log('calling IP: ' + ip);
        const POOL_URL = `http://${ip}:1317/thorchain/pools`
        let path = []
      //  console.log('loading ' + HEIGHT);
        console.log('url ' + POOL_URL);

        const result = await axios.get(POOL_URL)

        const pools:PoolBlock[] = result.data
        if (pools.length === 0){
            return [];
        }
        return pools
    } catch(e){
        console.log(e)
        return;
    }
}

const getPoolData = async(ASSET, HEIGHT, ISNEEDTIME: boolean, ip): Promise<PoolBlock> => {
    try {
        console.log('calling BUSD-BD1')
        let heightnew = HEIGHT
        if (ASSET === BUSD && parseInt(HEIGHT) <= 128909){
            console.log('calling BUSD-BD1 ' + heightnew)
            heightnew = '128909'
        }
        const POOL_URL = `http://${ip}:1317/thorchain/pool/${ASSET}?height=${heightnew}`
        const BLOCK_URL = `http://${ip}:27147/block?height=${heightnew}`;
        let path = []
        console.log('loading ' + HEIGHT);
        console.log('url ' + POOL_URL);

        path.push(axios.get(POOL_URL))
        if (ISNEEDTIME) {
            path.push(axios.get(BLOCK_URL))
        }
        const result = await axios.all(path)
        //    console.log(result)

        const pool:PoolBlock = result[0].data
        console.log(pool)
        if (ISNEEDTIME){
            const res:BlockData = result[1].data
            pool.time = res.result.block.header.time
        }
        pool.height = parseInt(HEIGHT)
        return pool

    } catch(e){
        console.log(e)
        return;
    }
}

const getPoolPairAtHeight = async(heights: [string], pool: string, isBUSDNeeded: boolean = true): Promise<PoolBlock[]> => {
    try {
        if (heights.length <= 0) {
            return []
        }
        const ip = await getNodeIp()
        const POOL_URL = `http://${ip}:1317/thorchain/pools`
        let path = []
        heights.forEach(value => {
            if (pool !== BUSD && isBUSDNeeded) {
                path.push(getPoolData(BUSD, value, false, ip))
            }
            path.push(getPoolData(pool, value, false, ip))
        })
       const result = await Promise.all(path)
       return  result
    } catch(e){
        console.log(e)
        return;
    }
}

const getNodeIp = async() => {
    try {
        const ENDPOINT = ":1317/thorchain/nodeaccounts"
        const seedlist  = await axios.get('https://chaosnet-seed.thorchain.info')
        let nodelist = await axios.get(`http://${seedlist.data[0]}${ENDPOINT}`)
        nodelist = nodelist.data;

        // Get pool address from all nodes.
        let activeAccounts = nodelist.filter(x => x.status == 'active').map(value => {
            return {ip: value.ip_address, status: value.status}
        })
        console.log(activeAccounts)
        const randomIp = activeAccounts[0]
        console.log(randomIp)
        return randomIp.ip

    } catch(e){
        console.log(e)
        return;
    }
}

const lastBlockHeight = async () => {
    let url = 'https://chaosnet-midgard.bepswap.com/v1/thorchain/lastblock'
    const result = await axios.get(url)
    const data: LastBlock = result.data
    if (data === undefined) {
        return
    }else {
        return data.thorchain
    }
}

const lowestUSDPool =  () => {
    const BUSD_POOl: PoolBlock = {
        balance_rune: 20000000000,
        balance_asset: 20000000000,
        asset: 'BNB.BUSD-BD1',
        pool_units: 20000000000,
        status: 'Bootstrap',
        height: 128935
    }
    return BUSD_POOl
}

const calculatePrice = (BUSD: PoolBlock, ASSET: PoolBlock) => {
    // What is the price of the asset in RUNE?
    let BUSD_RUNEPRICE =  BUSD.balance_rune / BUSD.balance_asset
    let BNB_RUNEPRICE =  ASSET.balance_rune / ASSET.balance_asset

    console.log(`BUSD PRICE IN RUNE: ${BUSD_RUNEPRICE.toFixed(4)}`)
    console.log(`BNB PRICE IN RUNE: ${BNB_RUNEPRICE.toFixed(4)}`)

    // What is the price of RUNE in the asset?
    let BUSD_ASSETPRICE = BUSD.balance_asset / BUSD.balance_rune
    let BNB_ASSETPRICE =   ASSET.balance_asset / ASSET.balance_rune

    console.log(`RUNE PRICE IN BUSD: ${BUSD_ASSETPRICE.toFixed(4)}`)
    console.log(`RUNE PRICE IN BNB: ${BNB_ASSETPRICE.toFixed(4)}`)

    // What is the price of one asset in another?
    let BNB_BUSDPRICE =  BNB_RUNEPRICE / BUSD_RUNEPRICE

    console.log(`BNB PRICE IN BUSD: ${BNB_BUSDPRICE.toFixed(4)}`)

    const price: PoolPrice = {
        BUSD_PRICE_IN_RUNE: BUSD_RUNEPRICE,
        ASSET_PRICE_IN_RUNE: BNB_RUNEPRICE,
        RUNE_PRICE_IN_BUSD: BUSD_ASSETPRICE,
        RUNE_PRICE_IN_ASSET: BNB_ASSETPRICE,
        ASSET_PRICE_IN_BUSD: BNB_BUSDPRICE,
    }
    return price
}

module.exports = {
    lastBlockHeight,
    getAllLastestPool,
    getPoolPairAtHeight
}
