generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model pools_history {
  time             DateTime
  id               Int      @default(autoincrement())
  asset            String
  assetdepth       Int
  assetroi         Float
  assetstakedtotal Int
  buyassetcount    Int
  buyfeeaverage    Float
  buyfeestotal     Int
  buyslipaverage   Float
  buytxaverage     Float
  buyvolume        Int
  pooldepth        Int
  poolfeeaverage   Float
  poolfeestotal    Int
  poolroi          Float
  poolroi12        Float
  poolslipaverage  Float
  poolstakedtotal  Int
  pooltxaverage    Float
  poolunits        Int
  poolvolume       Int
  poolvolume24hr   Int
  price            Float
  runedepth        Int
  runeroi          Float
  runestakedtotal  Int
  sellassetcount   Int
  sellfeeaverage   Float
  sellfeestotal    Int
  sellslipaverage  Float
  selltxaverage    Float
  sellvolume       Int
  staketxcount     Int
  stakerscount     Int
  stakingtxcount   Int
  status           String
  swapperscount    Int
  swappingtxcount  Int
  withdrawtxcount  Int

  @@id([id, time])
  @@index([id], name: "pools_history_event_id_idx")
  @@index([asset], name: "pools_history_pool_idx")
  @@index([time], name: "pools_history_time_idx")
}

model price_history {
  time      DateTime
  id        Int      @default(autoincrement())
  pricerune Float
  asset     String

  @@id([id, time])
  @@index([asset], name: "price_history_asset_idx")
  @@index([id], name: "price_history_id_idx")
  @@index([time], name: "price_history_time_idx")
}

model block_pool_history {
  id           Int      @default(autoincrement())
  time         DateTime
  asset        String
  balancerune  Int
  balanceasset Int
  poolunits    Int
  status       String
  height       Int

  @@id([id, time])
  @@index([asset], name: "block_pool_history_asset_idx")
  @@index([height], name: "block_pool_history_height_idx")
  @@index([id], name: "block_pool_history_idx")
  @@index([time], name: "block_pool_history_time_idx")
}

model block_pool_history_section {
  id           Int      @default(autoincrement())
  time         DateTime
  asset        String
  balancerune  Int
  balanceasset Int
  poolunits    Int
  status       String
  height       Int
  section      String

  @@id([id, time])
  @@index([asset], name: "block_pool_history_section_asset_idx")
  @@index([height], name: "block_pool_history_section_height_idx")
  @@index([id], name: "block_pool_history_section_idx")
  @@index([time], name: "block_pool_history_section_time_idx")
}

model block_pool_history_sections {
  id             Int      @default(autoincrement())
  time           DateTime
  asset          String
  balancerune    Int
  balanceasset   Int
  poolunits      Int
  status         String
  height         Int
  section        String
  busdpricerune  Float
  assetpricerune Float
  runepricebusd  Float
  runepriceasset Float
  assetpricebusd Float

  @@id([id, time])
  @@index([asset], name: "block_pool_history_sections_asset_idx")
  @@index([height], name: "block_pool_history_sections_height_idx")
  @@index([id], name: "block_pool_history_sections_idx")
  @@index([time], name: "block_pool_history_sections_time_idx")
}

model pool {
  id     Int    @id @default(autoincrement())
  asset  String @unique
  status String
}

model server_ips {
  id      Int      @default(autoincrement())
  time    DateTime
  address String

  @@id([id, time])
}

model staker_calculation {
  id                 Int      @default(autoincrement())
  time               DateTime
  address            String
  pool               String
  runestake          Int
  assetstake         Int
  poolunits          Int
  runewithdrawn      Int
  assetwithdrawn     Int
  totalstakedrune    Float
  totalstakedasset   Float
  totalstakedusd     Float
  totalunstakedrune  Float
  totalunstakedasset Float
  totalunstakedusd   Float
  status             String
  height             Int
  tx                 String
  type               String
  txid               Int
  firststake         Int
  laststake          Int

  @@id([id, time])
  @@index([txid], name: "staker_calcuation_txid")
  @@index([pool], name: "staker_calculation_asset_idx")
  @@index([height], name: "staker_calculation_height_idx")
  @@index([id], name: "staker_calculation_idx")
  @@index([time], name: "staker_calculation_time_idx")
}

model staker_pool {
  id      Int    @id @default(autoincrement())
  address String
  pool    String
  status  String
  lastid  Int

  @@unique([address, pool], name: "staker_pool_address_pool_key")
  @@index([id], name: "staker_pool_idx")
}

model sync_section {
  id        Int    @id @default(autoincrement())
  section   String
  lowheight Int
  maxheight Int
}

model test {
  sampleid Int @id @default(autoincrement())
  tetst    Int
}

model tx_block_pool {
  id           Int      @default(autoincrement())
  time         DateTime
  asset        String
  balancerune  Int
  balanceasset Int
  poolunits    Int
  status       String
  height       Int

  @@id([id, time])
  @@index([asset], name: "tx_block_pool_asset_idx")
  @@index([height], name: "tx_block_pool_height_idx")
  @@index([id], name: "tx_block_pool_idx")
  @@index([time], name: "tx_block_pool_time_idx")
}

model tx_history {
  time          DateTime
  id            Int      @default(autoincrement())
  pool          String
  target        String
  stakeunit     String
  assetamount   String
  targetamount  String
  type          String
  memo          String
  date          DateTime
  address       String
  tx            String
  status        String
  withdrawratio String
  height        Int

  @@id([id, time])
  @@unique([tx, address, status, time], name: "unique_tx_history_tx_address_status")
  @@index([pool], name: "tx_history_asset_idx")
  @@index([id], name: "tx_history_id_idx")
  @@index([time], name: "tx_history_time_idx")
}

model tx_history_copy {
  time          DateTime
  id            Int      @id
  pool          String
  target        String
  stakeunit     String
  assetamount   String
  targetamount  String
  type          String
  memo          String
  date          DateTime
  address       String
  tx            String
  status        String
  withdrawratio Int?
  height        String?
}

model working_copy {
  id      Int      @id @default(autoincrement())
  time    DateTime
  address String
  pool    String
  status  String

  @@index([id], name: "working_copy_idx")
}
